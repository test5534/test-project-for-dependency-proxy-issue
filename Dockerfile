ARG PREFIX

FROM ${PREFIX}/node:14-alpine

CMD ["node", "-e", "console.log('Running a command')"]
